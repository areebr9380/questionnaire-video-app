package com.mufakir.videoplay;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Vector;

public class playVid extends AppCompatActivity {
    int position;String arr[],link[];int size;int desirePostion;String ids[];
    private RequestQueue mQueue;
    RecyclerView recyclerView;Button b,b1,b2;
    Vector<YouTubeVideos> youtubeVideos = new Vector<YouTubeVideos>();
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_vid);
        Intent intent=getIntent();
        callVideos cv=new callVideos();
        String arr[]=cv.showTitles();
        String arr1[]=cv.showVideoLink();
        position = intent.getIntExtra("pos",-1);
        desirePostion=position;

        mQueue= Volley.newRequestQueue(this);
        jsonParse();

        b=(Button)findViewById(R.id.back);
        b1=(Button)findViewById(R.id.qa);
        b2=(Button)findViewById(R.id.next);



    }
    public void back(View v){
        position-=1;
        Intent intent=new Intent(getApplicationContext(),playVid.class);

        intent.putExtra("pos",position);
        finish();
        startActivity(intent);
    }
    public void next(View v){
        position+=1;
        Intent intent=new Intent(getApplicationContext(),playVid.class);

        intent.putExtra("pos",position);
        finish();
        startActivity(intent);

    }
    public void qa (View v){
        Intent intent=new Intent(getApplicationContext(),questionAnswer.class);
        String qaId=ids[position];
        intent.putExtra("pos",qaId);
        finish();
        startActivity(intent);

    }
    private void jsonParse(){
        size=0;
        String url="http://qalaqand.com/getVideosOnline.php";
        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray=response.getJSONArray("video");
                    size =jsonArray.length();
                    arr=new String[size];
                    link=new String[size];
                    ids=new String[size];
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject movie=jsonArray.getJSONObject(i);
                        String title=movie.getString("title");
                        String url=movie.getString("url");
                        String id1=movie.getString("id");
                        link[i]=url;
                        arr[i]=title;
                        ids[i]=id1;

//                        tv.setText(String.valueOf(jsonArray.length()));
//                        int total=Integer.parseInt(tv.getText().toString());



//                        tv.append(" The Release year is "+date+" Liter consumed is "+ltr+"\n\n");



                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Error in results",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                  displayThings(arr,link,desirePostion);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

            }
        });

        mQueue.add(request);
        int i;



    }
    public void displayThings(String[] arr1,String[] link,int desirePostion){
        if(desirePostion == 0){
            b.setEnabled(false);
        }
        if(desirePostion == (arr1.length-1)){
            b2.setEnabled(false);
        }
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        youtubeVideos.add( new YouTubeVideos("<iframe width=\"100%\" height=\"100%\" src=\""+link[desirePostion]+"\" frameborder=\"1\" allowfullscreen=\"1\"></iframe>") );

        VideoAdapter videoAdapter = new VideoAdapter(youtubeVideos);


        recyclerView.setAdapter(videoAdapter);
    }


}
