package com.mufakir.videoplay;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RequestQueue mQueue;
    String arr[],link[];ListView listView;int size;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
         listView=(ListView)findViewById(R.id.lv);
         size=0;

        mQueue= Volley.newRequestQueue(this);


        jsonParse();
    }
    private void jsonParse(){

        String url="http://qalaqand.com/getVideosOnline.php";
        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray=response.getJSONArray("video");
                    size =jsonArray.length();
                    arr=new String[size];
                    link=new String[size];
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject movie=jsonArray.getJSONObject(i);
                        String title=movie.getString("title");
                        String url=movie.getString("url");
                        link[i]=url;
                        arr[i]=title;
//                        tv.setText(String.valueOf(jsonArray.length()));
//                        int total=Integer.parseInt(tv.getText().toString());



//                        tv.append(" The Release year is "+date+" Liter consumed is "+ltr+"\n\n");



                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Error in results",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                displayThings(arr,link);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

            }
        });

        mQueue.add(request);
        int i;



    }
    public void displayThings(String[] arr,String[] link){
        this.arr=arr;
        this.link=link;
        int b;
        String result[]=new String[arr.length];

        for (b=0;b<arr.length;b++){
            result[b]=arr[b];
        }
        ArrayAdapter arrayAdapter=new ArrayAdapter(this,android.R.layout.simple_expandable_list_item_1,result);
        listView.setAdapter(arrayAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent=new Intent(getApplicationContext(),playVid.class);

                intent.putExtra("pos",position);
//                Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });


    }
    public void initialize_arrs(int size){ arr=new String[size];
    }
}
