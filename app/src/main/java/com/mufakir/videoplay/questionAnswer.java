package com.mufakir.videoplay;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class questionAnswer extends AppCompatActivity {
    private LinearLayout parentLinearLayout;
    private RequestQueue mQueue;
    String qaId;int size=0;
    String question[];
    String answer[];
    LayoutInflater inflater;
    LinearLayout m_ll;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_answer);
        parentLinearLayout = (LinearLayout) findViewById(R.id.parent_linear_layout);
        Intent intent=getIntent();
        qaId="";
        qaId=intent.getStringExtra("pos");
        int b=0;
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        m_ll = (LinearLayout) findViewById(R.id.parent_linear_layout);
        mQueue= Volley.newRequestQueue(this);
        jsonParse();



    }
    public void onAddField(View v) {
         inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.field, null);
        // Add the new row before the add field button.

        parentLinearLayout.addView(rowView, parentLinearLayout.getChildCount() - 1);
    }

    public void onDelete(View v) {
//        parentLinearLayout.removeView((View) v.getParent());
        ArrayList<EditText> list = new ArrayList<>();
        EditText editText=(EditText)findViewById(R.id.a);
        String j= editText.getText().toString();

        Toast.makeText(getApplicationContext(),String.valueOf(v.getId()),Toast.LENGTH_SHORT).show();
    }
    private void jsonParse(){

        String url="http://qalaqand.com/questionAnswer.php?id="+qaId;
        JsonObjectRequest request =new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray jsonArray=response.getJSONArray("video");
                    size =jsonArray.length();
                    question=new String[size];
                    answer=new String[size];
                    for(int i=0;i<jsonArray.length();i++){

                        JSONObject movie=jsonArray.getJSONObject(i);
                        String q=movie.getString("question");
                        String a=movie.getString("answer");
                        question[i]=q;
                        answer[i]=a;
//                        tv.setText(String.valueOf(jsonArray.length()));
//                        int total=Integer.parseInt(tv.getText().toString());



//                        tv.append(" The Release year is "+date+" Liter consumed is "+ltr+"\n\n");



                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(),"Error in results",Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
        displayThings(question,answer);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();

                Toast.makeText(getApplicationContext(),"Error",Toast.LENGTH_SHORT).show();

            }
        });

        mQueue.add(request);
        int i;



    }
    public void displayThings(String[] question,String[] answer){
        String qArr[]=question;
        final String aArr[]=answer;int b;
        for (b=0;b<question.length;b++){
            final int c=b;
            TextView text = new TextView(this);
            final EditText editText=new EditText(this);
            Button button=new Button(this);
            editText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            text.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            button.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            button.setText("Answer");
            text.setText(question[b]);

            m_ll.addView(text);
            m_ll.addView(editText);
            m_ll.addView(button);
            button.setId(R.id.but+b);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String newAnswer=editText.getText().toString();
                    String neutral=aArr[c].toLowerCase();
                    if(neutral.equals(newAnswer.toLowerCase())){

                        Toast.makeText(getApplicationContext(),"Correct Answer Keep it up",Toast.LENGTH_SHORT).show();

                    }
                    else {
                        Toast.makeText(getApplicationContext(),"Sorry! Wrong Answer",Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
}
